﻿using UnityEngine;
using UnityEditor;

struct VelocityState
{
    public float HorizontalVelocity;
    public float VerticalVelocity;

    public VelocityState(float hVelocity, float vVelocity)
    {
        this.HorizontalVelocity = hVelocity;
        this.VerticalVelocity = vVelocity;
    }

    public void Reset()
    {
        this.HorizontalVelocity = 00.0f;
        this.VerticalVelocity = 0.0f;
    }
}