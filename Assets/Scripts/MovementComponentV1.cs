﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class MovementComponentV1 : MonoBehaviour
{
    CapsuleCollider playerCollider;
    public float MaxSpeed = 6.5f;
    public float Acceleration = 30.0f;
    public float Deceleration = 60.0f;
    public LayerMask WhatIsFloor;
    public float Gravity = 9.8f;
    public float TerminalVelocity = 20.0f;
    public float FloorCheckDistance = 2.0f;
    public float JumpForce = 100.0f;
    public bool bIsJumping = false;

    // velocity
    VelocityState _velocityState;
    float VerticalVelocity = 0.0f;
    int MaxJumps = 2;
    int currentJumps = 0;

    // floor info
    GameObject floorGO;
    float floorHeight;
    public bool bOnFloor = false;
    float hoverDistance = 0.5f;
    float capsuleHalfHeight = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        playerCollider = GetComponent<CapsuleCollider>();
        capsuleHalfHeight = playerCollider.height;

        // reset states
        _velocityState.Reset();
    }

    // Update is called once per frame
    void Update()
    {
        // what does our movement look like?
        UpdateMovementStepNormal();
    }

    public void UpdateMovementStepNormal()
    {
        if (CanJump())
            StartJump(JumpForce);

        // check other steps and update
        if (bIsJumping)
            UpdateJumpState();
        else
            UpdateMovementStepOnGround();
    }

    void UpdateJumpState()
    {
        // what's our input
        float inputX = Input.GetAxisRaw("Horizontal");
        float targetSpeed = MaxSpeed * inputX;

        // update horizontal
        float newSpeed = ApplyAccelerationSigned(_velocityState.HorizontalVelocity, targetSpeed, Acceleration, Deceleration);
        _velocityState.HorizontalVelocity = newSpeed;

        // update vertical
        _velocityState.VerticalVelocity = UpdateVerticalVelocity();

        // apply it
        Vector3 currentPosition = transform.position;
        float currentVerticalVelocity = VerticalVelocity * Time.deltaTime;
        float currentHorizontalVelocity = _velocityState.HorizontalVelocity * Time.deltaTime;
        transform.position = currentPosition + new Vector3(0.0f, currentVerticalVelocity, currentHorizontalVelocity);

        bIsJumping = VerticalVelocity >= 0 ? true : false;
    }

    float UpdateVerticalVelocity()
    {
        // #TODO Remember that we also need to update Horizontal velocity
        Vector3 currentPosition = transform.position;
        float fallAmount = -Gravity * Time.deltaTime;
        VerticalVelocity += fallAmount;

        // clamp it
        VerticalVelocity = Mathf.Max(-TerminalVelocity, VerticalVelocity);


        return VerticalVelocity;
    }

    void UpdateMovementStepOnGround()
    {
        // what's our input
        float inputX = Input.GetAxisRaw("Horizontal");
        float targetSpeed = MaxSpeed * inputX;

        float newSpeed = ApplyAccelerationSigned(_velocityState.HorizontalVelocity, targetSpeed, Acceleration, Deceleration);
        _velocityState.HorizontalVelocity = newSpeed;
        
        // what about vertical velocity, should we update this separately to allow for
        // horizontal movement? Or should this only be done in falling or jumping?
        _velocityState.VerticalVelocity = UpdateVerticalVelocity();
       
        // apply it
        Vector3 currentPosition = transform.position;
        float currentVerticalVelocity = _velocityState.VerticalVelocity * Time.deltaTime;
        float currentHorizontalVelocity = _velocityState.HorizontalVelocity * Time.deltaTime;
        // if not on the floor then update our vertical location as well.
        Vector3 moveVector = bOnFloor ? new Vector3(0.0f, 0.0f, currentHorizontalVelocity) : new Vector3(0.0f, currentVerticalVelocity, currentHorizontalVelocity);

        // apply it
        Vector3 newPosition = currentPosition + moveVector;
        transform.position = newPosition;

        // populate our floor info
        SweepForFloor();
    }

    bool CanJump()
    {
        return Input.GetButtonDown("Jump") && currentJumps < MaxJumps;
    }

    void StartJump(float jumpForce)
    {
        bIsJumping = true;
        VerticalVelocity = jumpForce;
        currentJumps++;
    }

    public void SweepForFloor()
    {
        // create a capsuleCast from the top of the player to the bottom, look for floor objects
        float capsuleRadius = playerCollider.radius;
        RaycastHit floorHit;
        Vector3 p1 = transform.position + new Vector3(0.0f, capsuleHalfHeight, 0.0f);
        Vector3 p2 = p1 - new Vector3(0.0f, capsuleHalfHeight, 0.0f);

        // check if we have a floor
        bool bFloorFound = Physics.CapsuleCast(p1, p2, capsuleRadius, Vector3.down, out floorHit, FloorCheckDistance, WhatIsFloor);

        if (bFloorFound)
        {
            // populate floor info
            floorGO = floorHit.collider.gameObject;
            floorHeight = floorHit.point.y;
            
            Debug.Log("Floor: " + floorHit.collider.gameObject);
            //Debug.Log("Sweep hit: " + floorHit.collider.gameObject.name + " with height: " + floorHeight + 
            //    " distance: " + Mathf.Abs(GetPlayerBottom().y - floorHeight));
        }

        // update onFloor.
        bOnFloor = bFloorFound && Mathf.Abs(GetPlayerBottom().y - floorHeight) <= hoverDistance;

        // snap us to the floor if we are on the floor
        if (bOnFloor)
            SnapToFloor(floorHeight);
    }


    void SnapToFloor(float floorHeight)
    {
        currentJumps = 0;

        // calculate what our position on the floor would be. Offset based of our halfheight
        Vector3 newPos = transform.position;
        newPos.y = floorHeight + capsuleHalfHeight;

        // set it
        transform.position = newPos;
    }

    #region Acceleration Helpers
    // This version of the function assumes current and target are positive or zero.
    float ApplyAcceleration(float current, float target, float acceleration, float deceleration)
    {
        if (target > current)
        {
            float newValue = current + acceleration * Time.deltaTime;
            return Mathf.Min(newValue, target);
        }
        else if (target < current)
        {
            float newValue = current - deceleration * Time.deltaTime;
            return Mathf.Max(newValue, target);
        }
        else
            return current;
    }

    float ApplyAcceleration(float current, float target, float acceleration)
    {
        return ApplyAcceleration(current, target, acceleration, acceleration);
    }

    float ApplyAccelerationSigned(float current, float target, float acceleration)
    {
        return ApplyAccelerationSigned(current, target, acceleration, acceleration);
    }

    // This version of the function deals with current and target being different signs. If we are crossing over the sign, then we will decelerate until we
    // cross over zero and then accelerate.
    float ApplyAccelerationSigned(float current, float target, float acceleration, float deceleration)
    {
        float currentSign = Mathf.Sign(current);
        float targetSign = Mathf.Sign(target);

        if (currentSign == targetSign || currentSign == 0.0f)
        {
            // we are either on the same side or going from 0 to the value, so use acceleration and deceleration
            float valueAbs = ApplyAcceleration(Mathf.Abs(current), Mathf.Abs(target), acceleration, deceleration);
            return targetSign * valueAbs;
        }
        else
        {
            // we are changing signs (or targeting zero), in which case we always deceleration (until we cross over)
            float diff = current - target;
            float diffAbs = Mathf.Abs(diff);
            float diffSign = Mathf.Sign(diff);
            float newDelta = ApplyAcceleration(0.0f, diffAbs, deceleration);
            return current - diffSign * newDelta;
        }
    }
    #endregion

    #region Player Body Location Helpers
    // helper functions
    Vector3 GetPlayerBottom()
    {
        // origin is at center of player, subtract our half height to get the bottom
        return transform.position - new Vector3(0.0f, capsuleHalfHeight, 0.0f);
    }

    Vector3 GetPlayerTop()
    {
        return transform.position + new Vector3(0.0f, capsuleHalfHeight, 0.0f);
    }

    #endregion

    #region DebugDrawing
    private void OnDrawGizmos()
    {
        // Draw player bottom
        //float capsuleHalfHeight = GetComponent<CapsuleCollider>().height;
        //Gizmos.color = Color.red;
        //Gizmos.DrawWireSphere(transform.position - new Vector3(0.0f, capsuleHalfHeight, 0.0f), 0.25f);
    }
    #endregion
}
